Gigya Toolbar


Add the Gigya Toolbar for Free and Increase Traffic and Engagement!

    * Get return traffic when users share and tweet from the toolbar.
    * Supports 80 social destinations and includes a Facebook fan page and Twitter updates buttons.
    * FREE and takes only 2 minutes to add to any site - no coding needed.
    * The toolbar does not affect page load time! It loads only after the page is fully loaded.
    * Get paid for searches made from the toolbar (coming soon)


The Social Toolbar includes a richness of features and we are adding more features every month:

    * Connect to one or more social networks - The toolbar currently supports connecting to FaceBook, Twitter, MySpace and Yahoo and sharing into additional 70+ destinations.
    * Facebook �Fan Page� button � Users can check out your page updates and can become a fan with one click.
    * Twitter updates button � Users can check out your recent tweets right from your toolbar.
    * Update status/Tweet - Users can update status or tweet right from the toolbar. Site owners can set the default message text.
    * Google site search � Users can search the content of your site.
    * Share � One click share to over 80 social destinations.
    * Email � Users can email friends and even import contacts from Gmail, Hotmail, Yahoo and AOL.
    * Subscribe to feed - Lets users subscribe to your feed.
    * Themes - use default or Blue themes. More themes coming soon.
    * Hide/unhide toolbar
    * Chat with social network friends directy from the toolbar - Coming soon!

