<?php

/**
 * @file
 * This module allows sites to use the Gigya Toolbar for Free and Increase Traffic and Revenues!
 *
 * Development of this module is implemented by Gigya.
 *
 */

/**
 * Constants defined for gigyaToolbar system settings
 */

define('GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR', 'Customizing the Toolbar');
define('GIGYATOOLBAR_AFTER_INIT', 'gigyaToolbar_AFTER_INIT');
define('GIGYATOOLBAR_PARTNER_FIELD', 'gigyaToolbar_partner_field');
define('GIGYATOOLBAR_PARTNER', 'gigyaToolbar_partner');
define('GIGYATOOLBAR_FACEBOOK_ID_FIELD', 'gigyaToolbar_facebook_id_field');
define('GIGYATOOLBAR_FACEBOOK_ID', 'gigyaToolbar_facebook_id');              
define('GIGYATOOLBAR_TWITTER_USERNAME_FIELD', 'gigyaToolbar_twitter_name_field');
define('GIGYATOOLBAR_TWITTER_USERNAME', 'gigyaToolbar_twitter_name');                          
define('GIGYATOOLBAR_RSS_URL_FIELD', 'gigyaToolbar_rss_url_field');
define('GIGYATOOLBAR_RSS_URL', 'gigyaToolbar_rss_url');
define('GIGYATOOLBAR_THEME_FIELD', 'gigyaToolbar_theme_field');
define('GIGYATOOLBAR_THEME', 'gigyaToolbar_theme');
define('GIGYATOOLBAR_HIDE_SEARCH_FIELD', 'gigyaToolbar_hide_search_field');
define('GIGYATOOLBAR_HIDE_SEARCH', 'gigyaToolbar_hide_search');
define('GIGYATOOLBAR_STATUSMESSAGE_FIELD', 'gigyaToolbar_statusMessage_field');
define('GIGYATOOLBAR_STATUSMESSAGE', 'gigyaToolbar_statusMessage');
define('GIGYATOOLBAR_SUBJECT_FIELD', 'gigyaToolbar_subject_field');
define('GIGYATOOLBAR_SUBJECT', 'gigyaToolbar_subject');
define('GIGYATOOLBAR_BODY_FIELD', 'gigyaToolbar_body_field');
define('GIGYATOOLBAR_BODY', 'gigyaToolbar_body');
define('GIGYATOOLBAR_REGISTER_LINK_URL', 'https://www.gigya.com/site/Register.aspx');
define('GIGYATOOLBAR_ACC_SET_PAGE_URL', 'https://www.gigya.com/site/partners/updatedetails.aspx');
define('GIGYATOOLBAR_FB_BARISTA_URL', 'http://www.facebook.com/pages/Barista-Kids/116090169739');
define('GIGYATOOLBAR_WIKI_URL', 'http://wiki.gigya.com/030_Gigya_Socialize_API_2.0/Social_Toolbar');

/**
 *
 */
function gigyaToolbar_admin_form() {		     
  $form = array();
  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize the toolbar'),
    '#description' => t('Set the optional parameters for the Gigya toolbar.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -4,
  );
  
  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_PARTNER_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Your Gigya partner ID'),
    '#description' => '<p>' . t('If you want to track analytics for your toolbar, you must first sign up for a Gigya account <a href="@sinup-page"> here</a> and enter your Gigya partner ID where it says "Your Gigya ID" above. You can find your partner ID in the <a href="@accset-page"> account setting page</a>. This allows Gigya to track your toolbar performance and pay you when users search from your toolbar.', array('@sinup-page' => url(GIGYATOOLBAR_REGISTER_LINK_URL), '@accset-page' => url(GIGYATOOLBAR_ACC_SET_PAGE_URL))) . '</p>',  
    '#default_value' => variable_get(GIGYATOOLBAR_PARTNER, 'YOUR GIGYA ID'),
    '#weight' => -45,
  );
    
  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_FACEBOOK_ID_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Add Facebook fan page button'),
    '#description' => '<p>' . t('If you have a page on Facebook, you can add a button that when clicked, shows your page updates and fans and lets users become fans of your page. To add the button, go to your page on Facebook and grab the page ID which appears at the end of the URL.') .'</p><p>' . t(' Example: <a href="@fbbarista-page">http://www.facebook.com/pages/Barista-Kids/116090169739</a> - Enter this ID (116090169739 in this example) where it says "Fan Page ID" above.',array('@fbbarista-page' => url(GIGYATOOLBAR_FB_BARISTA_URL))) . '</p>',
    '#default_value' => variable_get(GIGYATOOLBAR_FACEBOOK_ID, 'Fan Page ID'),    
    '#weight' => -40,
  );

    $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_TWITTER_USERNAME_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Add a Twitter updates button'),
    '#description' => t('You can add a button that when clicked shows the recent tweets from your account. To add the button, enter your twitter username where it says "Twitter username" above.'),
    '#default_value' => variable_get(GIGYATOOLBAR_TWITTER_USERNAME, 'Twitter username'),    
    '#weight' => -35,
  );
  
    $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_RSS_URL_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('RSS button'),
    '#description' => t('To add a button that enables users to subscribe to your RSS feed, enter your RSS URL where in says "RSS URL" above.'),
    '#default_value' => variable_get(GIGYATOOLBAR_RSS_URL, 'RSS URL'),    
    '#weight' => -30,
  );
  
  $gigyaToolbar_ThemesArr = array('blue'=>'blue', ''=>'default theme');
  
    $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_THEME_FIELD] = array(
    '#type' => 'select',
    '#options' => $gigyaToolbar_ThemesArr,
    '#title' => t('Theme'),
    '#description' => t('To change the gigyaToolbar theme, select a theme name above (other themes are coming soon).'),
    '#default_value' => variable_get(GIGYATOOLBAR_THEME, 'blue'),    
    '#weight' => -25,
  );  
  
    $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_HIDE_SEARCH_FIELD] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide search box'),
    '#description' => t('To hide the search box, click on the checkboxe above.'),
    '#default_value' => variable_get(GIGYATOOLBAR_HIDE_SEARCH_FIELD, 'FALSE'),    
    '#weight' => -23,
  );  
        
  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_STATUSMESSAGE_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Default status message'),
    '#description' => t('You can set a message relating to your site as the default status/tweet. The default message will appear in the status update box. Users will also have the option to change it before they send an update. To set your own custom message, replace the "Default status message" above with your message. Use the following placeholders in the text above. This text will be used when others share your article. %site_name - Replaced with the name of your site. %node_url - Replaced with the link to the post being shared. %title - Replaced with the title of the post being shared.'),
    '#default_value' => variable_get(GIGYATOOLBAR_STATUSMESSAGE, 'Reading %title at %node_url'),    
    '#weight' => -20,
  );

  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_SUBJECT_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Default email subject'),
    '#description' => t('You can set the default subject for emails sent from the toolbar. To set your custom email subject, replace the "Default email subject" above with the subject text of your choice. Use the following placeholders in the text above. This text will be used when others share your article. %site_name - Replaced with the name of your site. %node_url - Replaced with the link to the post being shared. %title - Replaced with the title of the post being shared.'),
    '#default_value' => variable_get(GIGYATOOLBAR_SUBJECT, 'Nice article - %title'),    
    '#weight' => -15,
  );

  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR][GIGYATOOLBAR_BODY_FIELD] = array(
    '#type' => 'textfield',
    '#title' => t('Default email body'),
    '#description' => t('You can set the default body for emails sent from the toolbar. To set your custom email body, replace the "Default email body" above with the body text of your choice.  Use the following placeholders in the text above. This text will be used when others share your article. %site_name - Replaced with the name of your site.  %node_url - Replaced with the link to the post being shared. %title - Replaced with the title of the post being shared.'),
    '#default_value' => variable_get(GIGYATOOLBAR_BODY, 'Take a look at this article <a href="%node_url">%title</a> from %site_name'),
    '#weight' => -10,
  );  
  
  $form[GIGYATOOLBAR_CUSTOMIZING_THE_TOOLBAR]['gs_readmore'] = array(
  	'#type' => 'item', 	
  	'#title' => t('Additional info'),
    '#value' => '<p>' . t('You can read more about customizing the Gigya Toolbar <a href="@toolbarwiki-page"> here.</a>', array('@toolbarwiki-page' => url(GIGYATOOLBAR_WIKI_URL))) . '</p>',   

  
    '#weight' => -5,
  );    

  $form['#submit'][0] = 'gigyaToolbar_save_conf_submit';   
  return system_settings_form($form);
}

function gigyaToolbar_save_configuration($val) {
	variable_set(GIGYATOOLBAR_PARTNER,$val[GIGYATOOLBAR_PARTNER_FIELD]);	
	variable_set(GIGYATOOLBAR_FACEBOOK_ID,$val[GIGYATOOLBAR_FACEBOOK_ID_FIELD]);
	variable_set(GIGYATOOLBAR_TWITTER_USERNAME,$val[GIGYATOOLBAR_TWITTER_USERNAME_FIELD]);
	variable_set(GIGYATOOLBAR_RSS_URL,$val[GIGYATOOLBAR_RSS_URL_FIELD]);
    variable_set(GIGYATOOLBAR_THEME,$val[GIGYATOOLBAR_THEME_FIELD]);
    variable_set(GIGYATOOLBAR_HIDE_SEARCH,$val[GIGYATOOLBAR_HIDE_SEARCH_FIELD]);		
	variable_set(GIGYATOOLBAR_STATUSMESSAGE,$val[GIGYATOOLBAR_STATUSMESSAGE_FIELD]);
	variable_set(GIGYATOOLBAR_SUBJECT,$val[GIGYATOOLBAR_SUBJECT_FIELD]);
	variable_set(GIGYATOOLBAR_BODY,$val[GIGYATOOLBAR_BODY_FIELD]);	
} 

function gigyaToolbar_save_conf_submit($form, &$form_state) {	
	$currentValues = $form_state['values'];	                     
	if ( is_array($currentValues)) { 				
		if ($currentValues['op'] == t('Save configuration') ) {
			gigyaToolbar_save_configuration($currentValues);						            
			return;
		}					
	} 
}

